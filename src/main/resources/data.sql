
DROP TABLE IF EXISTS PRICES;

CREATE TABLE PRICES ( IDPRICES INT AUTO_INCREMENT  PRIMARY KEY,
                      BRAND_ID INT,
                      PRODUCT_ID INT,
                      START_DATE TIMESTAMP,
                      PRIORITY INT ,
                      PRICE FLOAT,
                      CURR VARCHAR(5) ,
                      END_DATE TIMESTAMP,
                      PRICE_LIST INT
                    );


insert into PRICES (BRAND_ID,PRODUCT_ID,START_DATE,PRIORITY,PRICE,CURR,END_DATE,PRICE_LIST) values(1,35455,parsedatetime('2020-06-14-00.00.00','yyyy-MM-dd-hh.mm.ss'), 0, 35.50,'EUR', parsedatetime('2020-12-31-23.59.59','yyyy-MM-dd-hh.mm.ss'),1);
insert into PRICES (BRAND_ID,PRODUCT_ID,START_DATE,PRIORITY,PRICE,CURR,END_DATE,PRICE_LIST) values(1,35455,parsedatetime('2020-06-14-15.00.00','yyyy-MM-dd-hh.mm.ss'), 1, 25.45,'EUR', parsedatetime('2020-06-14-18.30.00','yyyy-MM-dd-hh.mm.ss'),2);
insert into PRICES (BRAND_ID,PRODUCT_ID,START_DATE,PRIORITY,PRICE,CURR,END_DATE,PRICE_LIST) values(1,35455,parsedatetime('2020-06-15-00.00.00','yyyy-MM-dd-hh.mm.ss'), 1, 30.50,'EUR', parsedatetime('2020-06-15-11.00.00','yyyy-MM-dd-hh.mm.ss'),3);
insert into PRICES (BRAND_ID,PRODUCT_ID,START_DATE,PRIORITY,PRICE,CURR,END_DATE,PRICE_LIST) values(1,35455,parsedatetime('2020-06-15-16.00.00','yyyy-MM-dd-hh.mm.ss'), 0, 38.95,'EUR', parsedatetime('2020-12-31-23.59.59','yyyy-MM-dd-hh.mm.ss'),4);



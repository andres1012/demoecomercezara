package com.zara.ecomercezara.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO {

    private Long productId;
    private Integer brandId;
    private Short priceList;
    private Float price;
    private LocalDateTime startDate;
    private LocalDateTime endDate;



}
package com.zara.ecomercezara.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "PRICES")
public class Price implements Serializable {


@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)

private Integer brandId;
@Column(name = "START_DATE")
private LocalDateTime startDate;

@Column(name = "END_DATE")
private LocalDateTime endDate;

@Column(name = "PRICE_LIST")
private  Short priceList;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Short getPriceList() {
        return priceList;
    }

    public void setPriceList(Short priceList) {
        this.priceList = priceList;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Short getPriority() {
        return priority;
    }

    public void setPriority(Short priority) {
        this.priority = priority;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    @Column(name = "PRODUCT_ID")
private Long productId;

@Column(name = "PRIORITY")
private Short priority;

@Column(name = "PRICE")
private Float price;

@Column(name = "CURR")
private String curr;







}

package com.zara.ecomercezara.controler;


import com.zara.ecomercezara.dto.RequestDTO;
import com.zara.ecomercezara.dto.ResponseDTO;
import com.zara.ecomercezara.service.PriceServices;
import com.zara.ecomercezara.util.DateConverter;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Consumer;


@RestController
@RequestMapping("/price")
public class PriceController {

	@Autowired
	PriceServices priceServices;
	private Consumer<ResponseDTO> responseDTOConsumer;

	@GetMapping(value = "/list" , produces ="application/json; charset=UTF-8")
	@ResponseStatus(HttpStatus.OK)
	public List<ResponseDTO> getPrice(@RequestParam (value = "idBrand") Integer idBrand,
									  @RequestParam (value ="apliDate") String apliDate,
									  @RequestParam (value = "idProduct") Long idProduct )throws Exception {


		LocalDateTime localDateTime = DateConverter.getLocalDateTime(apliDate,"yyyy-MM-dd-HH.mm.ss");

		RequestDTO requestDTO = new RequestDTO();
		requestDTO.setProductId(idProduct);
		requestDTO.setAplicationDate(localDateTime);
		requestDTO.setBrandId(idBrand);

		List<ResponseDTO> responses = priceServices.getPrices(requestDTO);


		return responses;
	}


}

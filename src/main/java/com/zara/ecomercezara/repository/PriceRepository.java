package com.zara.ecomercezara.repository;

import com.zara.ecomercezara.entity.Price;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;


public interface PriceRepository extends CrudRepository<Price, Long> {



    @Query("FROM Price p"
            + " WHERE p.productId = :idProduct"
            + " and p.brandId = :idBrand"
            + " and p.startDate <= :apDate"
            + " and p.endDate >= :apDate")
    List<Price> findByDateProductAndBrand(@Param("apDate") LocalDateTime time,
                                    @Param("idProduct") Long idProduct,
                                    @Param("idBrand") Integer idBrand);



}

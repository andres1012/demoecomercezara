package com.zara.ecomercezara.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public  class DateConverter {

    /**
     *
     * @param dateTime string of datatimeformat
     * @param mask format to transform
     * @return LocalDataTime with mask format
     */
    public static LocalDateTime  getLocalDateTime(String dateTime, String mask) throws Exception{

       try {
           DateTimeFormatter formatter = DateTimeFormatter.ofPattern(mask);
           String hora = dateTime;
           LocalDateTime localDateTime = LocalDateTime.parse(dateTime, formatter);
           return  localDateTime;

       }catch(Exception e){

            throw new Exception("Format converter error");

           }

    }


}

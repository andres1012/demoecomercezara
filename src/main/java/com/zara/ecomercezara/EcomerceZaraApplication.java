package com.zara.ecomercezara;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title = "API Chalenge to Zara "
				, description = "End-Point to query of prices")
)
public class EcomerceZaraApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcomerceZaraApplication.class, args);
	}

}

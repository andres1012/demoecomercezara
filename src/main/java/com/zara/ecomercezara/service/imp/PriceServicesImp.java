package com.zara.ecomercezara.service.imp;

import com.zara.ecomercezara.dto.RequestDTO;
import com.zara.ecomercezara.dto.ResponseDTO;
import com.zara.ecomercezara.entity.Price;
import com.zara.ecomercezara.repository.PriceRepository;
import com.zara.ecomercezara.service.PriceServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PriceServicesImp implements PriceServices {

    @Autowired
    PriceRepository priceRepository;

    @Override
    public List<ResponseDTO> getPrices(RequestDTO requestDTO) {

        List<Price> prices = priceRepository.findByDateProductAndBrand( requestDTO.getAplicationDate(),requestDTO.getProductId(),requestDTO.getBrandId());

        return prices.stream().map(price -> new ResponseDTO(price.getProductId(), price.getBrandId(), price.getPriceList(), price.getPrice(), price.getStartDate(), price.getEndDate())).collect(Collectors.toList());



    }

    }





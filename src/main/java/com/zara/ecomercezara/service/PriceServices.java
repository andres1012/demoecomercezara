package com.zara.ecomercezara.service;

import com.zara.ecomercezara.dto.RequestDTO;
import com.zara.ecomercezara.dto.ResponseDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PriceServices {

    List<ResponseDTO> getPrices(RequestDTO requestDTO);

}


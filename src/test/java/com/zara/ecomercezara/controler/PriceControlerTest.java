package com.zara.ecomercezara.controler;


import org.junit.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PriceControlerTest extends  AbstractControllerTest {

    @Test
    public void shouldReturnFoundGet() throws Exception {


        mockMvc.perform(get("/price/list")
                .param("apliDate", "2020-06-14-16.00.00")
                .param("idBrand", BRAND_ID.toString())
                .param("idProduct", PRODUCT_ID.toString())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


    }
}
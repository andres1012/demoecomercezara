package com.zara.ecomercezara.controler;

import com.zara.ecomercezara.service.PriceServices;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest
public abstract class AbstractControllerTest {

	public static final Long PRODUCT_ID = 1l;
	public static final Integer BRAND_ID = 1;

	@Autowired
	protected MockMvc mockMvc;

	@MockBean
	protected PriceServices priceServices;

	@Before
	public void setUp() {
		Mockito.reset(priceServices);
	}

}

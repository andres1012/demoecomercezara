package com.zara.ecomercezara.service.imp;


import com.zara.ecomercezara.dto.RequestDTO;
import com.zara.ecomercezara.dto.ResponseDTO;
import com.zara.ecomercezara.service.PriceServices;
import com.zara.ecomercezara.util.DateConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PriceServicesTest {

	@Autowired
	PriceServices priceServices;

	@Test
	public void requestWithDate10Day14() throws Exception {
		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2020-06-14-10.00.00","yyyy-MM-dd-HH.mm.ss") ;
		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be one comment", responseDTO, hasSize(1));

	}

	@Test
	public void requestWithDate16Day14() throws Exception {

		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2020-06-14-16.00.00","yyyy-MM-dd-HH.mm.ss") ;

		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be 2 comment", responseDTO, hasSize(2));

	}

	@Test
	public void requestWithDate21Day14() throws Exception {

		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2020-06-14-21.00.00","yyyy-MM-dd-HH.mm.ss") ;

		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be 1 responseDTO", responseDTO, hasSize(1));

	}

	@Test
	public void requestWithDate10Day15() throws Exception {

		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2020-06-15-10.00.00","yyyy-MM-dd-HH.mm.ss") ;

		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be 2 responseDTO", responseDTO, hasSize(2));

	}

	@Test
	public void requestWithDate21Day16() throws Exception {

		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2020-06-16-21.00.00","yyyy-MM-dd-HH.mm.ss") ;

		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be 2 responseDTO", responseDTO, hasSize(2));

	}


	@Test
	public void requestWithDateMinor() throws Exception {

		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2020-02-16-21.00.00","yyyy-MM-dd-HH.mm.ss") ;

		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be 0 responseDTO", responseDTO, hasSize(0));

	}



	@Test
	public void requestWithDateMajor() throws Exception {

		RequestDTO requestDTO = new RequestDTO();

		LocalDateTime localDateTime = DateConverter.getLocalDateTime("2021-01-16-21.00.00","yyyy-MM-dd-HH.mm.ss") ;

		requestDTO.setAplicationDate(localDateTime);

		requestDTO.setBrandId(1);
		requestDTO.setProductId(35455L);

		List<ResponseDTO> responseDTO = priceServices.getPrices(requestDTO);

		assertThat("There should be 0 responseDTO", responseDTO, hasSize(0));

	}

}